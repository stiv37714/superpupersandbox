package Experiments;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Generics {
    public static void main(String[] args) {
// До Java 5
   /*   List list1 = new ArrayList();
        Animal ourAnimal = new Animal();
        list1.add("dasf");
        list1.add("ddfsaf");
        list1.add("daafgg");
        list1.add("das");
        list1.add(ourAnimal);
        String s = ((ArrayList) list1).get(1);
        System.out.println(s);

    }
}*/

        //java 7+
        List<String> list2 = new ArrayList<>();

        list2.add("dsad");
        list2.add("d");
        list2.add("dasasd");
        list2.add("daaaaasd");
        list2.add("dasdzbcghhfgh");

        String s1 = list2.get(2);
        System.out.println(s1);
    }
}
        class Animal {

        }

