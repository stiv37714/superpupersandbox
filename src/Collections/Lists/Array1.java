package Collections.Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Array1 {
    private String name;
    private int id;

    public Array1(String name, int id) {
        this.name = name;
        this.id = id;
    }


    public static void main(String[] args) {
        List<Array1> nArray = new ArrayList<>();
        Array1 tom = new Array1("Tom", 1);
        Array1 phil = new Array1("Phil", 2);
        Array1 tim = new Array1("Tim", 3);
        Array1 jack = new Array1("Jack", 4);
        Array1 said = new Array1("Said", 5);
        Array1 rob = new Array1("Rob", 6);
        Array1 john = new Array1("John", 7);
        Array1 bill = new Array1("Bill", 8);


        nArray.add(tom);
        nArray.add(tim);
        nArray.add(jack);
        nArray.add(john);
        nArray.add(said);
        nArray.add(phil);
        nArray.add(rob);

        Array1 m = nArray.get(3);
        System.out.println(m);
        System.out.println(nArray.toString());
        nArray.set(0, bill);
        System.out.println(nArray.toString());

    }
        @Override
        public String toString () {
            return "Array1{" +
                    "name='" + name + '\'' +
                    ", id=" + id +
                    '}';
        }
    }
