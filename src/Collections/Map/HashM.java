package Collections.Map;

import java.util.HashMap;
import java.util.Map;

public class HashM {
    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "Раз");
        map.put(2, "Два");
        map.put(3, "Три");
        map.put(4, "Четыре");
        System.out.println(map);
        map.put(3, "Троечка");
        System.out.println(map);

        System.out.println(map.get(1));

        for (Map.Entry <Integer, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " - - " + entry.getValue());
        }

    }
}
