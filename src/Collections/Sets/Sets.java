package Collections.Sets;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class Sets {
    public static void main(String[] args) {
        Set<String> hashSet = new HashSet<>();
        Set<String> linkedHashSet = new LinkedHashSet<>();
        Set<String> treeSet = new TreeSet<>();

        hashSet.add("Стас");
        hashSet.add("Коля");
        hashSet.add("Миша");
        hashSet.add("Вова");
        hashSet.add("Димон");

        linkedHashSet.add("Стас");
        linkedHashSet.add("Коля");
        linkedHashSet.add("Миша");
        linkedHashSet.add("Вова");
        linkedHashSet.add("Димон");

        treeSet.add("Стас");
        treeSet.add("Коля");
        treeSet.add("Миша");
        treeSet.add("Вова");
        treeSet.add("Димон");

        for (String name: hashSet
             ) {
            System.out.println(name);
        }
        System.out.println();
        for (String name: linkedHashSet
        ) {
            System.out.println(name);
    }
        System.out.println();
        for (String name: treeSet
        ) {
            System.out.println(name);
        }

        //объединение множеств

        Set<Integer>  set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(2);
        set1.add(3);
        set1.add(4);
        set1.add(5);
        set1.add(6);

        Set<Integer>  set2 = new HashSet<>();

        set1.add(3);
        set1.add(4);
        set1.add(5);
        set1.add(6);
        set1.add(7);
        set1.add(8);
        set1.add(9);



        set1.addAll(set2);
}}
