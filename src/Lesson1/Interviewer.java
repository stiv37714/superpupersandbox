package Lesson1;

class Interviewer extends ProductPerson {
 protected Interviewer(String name, int age, Sex sex) {
        super(name, age, sex);
    }

    //методы работы с резюме
    protected void getCv() {
        System.out.println("Получил резюме кандидата");

    }

    protected void readCv() {
        System.out.println("Прочитал резюме кандидата");

    }

    protected void giveCvApproav() {
        System.out.println("Одобрил резюме кандидата");

    }

    protected void rejectCv() {
        System.out.println("Отклонил резюме кандидата");

    }
    //методы интервью и решения

    protected void makeAnInterview() {
        System.out.println("Прособеседовал кандидата");

    }

    protected void approavCandidate() {
        System.out.println("Одобрил кандидата по итогам интервью");

    }

    protected void rejectCandidate() {
        System.out.println("Отказал кандидату по итогам интервью");
    }


}

