package Lesson1;

class HeadOfDepartment extends ProductPerson {

   protected HeadOfDepartment(String name, int age, Sex sex) {
        super(name, age, sex);
    }

    protected void sayHire() {
        System.out.println("Директор департамента предложил нанять нового сотрудника");
    }

    protected void sayFire() {
        System.out.println("Директор департамента отклонил одобренного кандидата");

    }

    protected void getCv() {
        System.out.println("Директор департамента увидел резюме кандидата");
    }
}
