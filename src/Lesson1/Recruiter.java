package Lesson1;

class Recruiter extends HumanResources {


    protected Recruiter(String name, int age, Sex sex) {
        super(name, age, sex);
    }

    protected void makeInterview() {
        System.out.println("HR-менеджер ведет интервью с кандидатом");
    }

    protected void talkWithInterviewer() {
        System.out.println("HR-менеджер общается с техническим специалистом");
    }

    protected void talkWithPrM() {
        System.out.println("HR-менеджер общается с руководителем проекта");
    }


    @Override
    protected void findCv() {
        System.out.println("HR-менеджер нашел резюме");

    }

    @Override
    protected void declineCv() {
        System.out.println("HR-менеджер отклонил резюме");
    }

    @Override
    protected void approveCv() {
        System.out.println("HR-менеджер одобрил резюме");

    }
}
