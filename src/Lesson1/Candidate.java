package Lesson1;

class Candidate {
    private String name;
    private Sex sex;
    private int age;
    private int yearsOfWork;

    protected void lookAtVacansy() {
        System.out.println("Кандидат увидел вакансию");
    }

    protected void responseToVacansy() {
        System.out.println("Кандидат откликнулся на вакансию");
    }

    protected void goToTheInterview() {
        System.out.println("Кандидат пошел на интервью");
    }

    protected void goodInterview() {
        System.out.println("Кандидат успешно прошел интервью");
    }

    protected void badInterview() {
        System.out.println("Кандидат завалил интервью и не прошел его");
    }

    protected void getOffer() {
        System.out.println("Кандидат получил предложение о работе");
    }

    protected void acceptOffer() {
        System.out.println("Кандидат принял предложение о работе");
    }

    protected void declineOffer() {
        System.out.println("Кандидат отклонил предложение о работе");
    }

}
