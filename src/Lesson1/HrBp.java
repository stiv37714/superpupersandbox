package Lesson1;

class HrBp extends HumanResources {
    protected HrBp(String name, int age, Sex sex) {
        super(name, age, sex);
    }

    protected void sayHire() {
        System.out.println("HRBP подтвердил найм кандидата в компанию");
    }

    protected void sayFire() {
        System.out.println("HRBP не согласовал прием кандидата");
    }

    @Override
    protected void findCv() {
        System.out.println("HRBP нашел резюме кандидата");

    }

    @Override
    protected void declineCv() {
        System.out.println("HRBP отказал рассматривать кандидата после прочтения его резюме");

    }

    @Override
    protected void approveCv() {
        System.out.println("HRBP одобрил резюме кандидата и рекомендовал прособеседовать последнего");

    }
}
