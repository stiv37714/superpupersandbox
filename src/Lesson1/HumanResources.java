package Lesson1;

import java.io.IOException;

abstract class HumanResources {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws IOException {
        while (true) {
            this.age = age;
            if (age < 0) {
                throw new IOException();
            }
        }
    }


    private Sex sex;

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public HumanResources(String name, int age, Sex sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    protected abstract void findCv();

    protected abstract void declineCv();

    protected abstract void approveCv();

}
