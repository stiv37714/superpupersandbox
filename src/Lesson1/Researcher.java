package Lesson1;

class Researcher extends HumanResources {

    protected Researcher(String name, int age, Sex sex) {
        super(name, age, sex);
    }

    protected void callToCandidate() {
        System.out.println("Ресечер позвонил кандидату");
    }

    protected void inviteCandidate() {
        System.out.println("Ресечер пригласил кандидата на интервью");
    }

    protected void sayGoodbyeCandidate() {
        System.out.println("Ресечер выслал отказ кандидату");
    }

    protected void callToRecruiter() {
        System.out.println("Ресечер связался с HR-менеджером");
    }


    @Override
    protected void findCv() {
        System.out.println("Ресечер нашел новое резюме");

    }

    @Override
    protected void declineCv() {
        System.out.println("Ресечер отклонил новое найденное резюме");

    }

    @Override
    protected void approveCv() {
        System.out.println("Ресечер одобрил новое найденное резюме");
    }
}
