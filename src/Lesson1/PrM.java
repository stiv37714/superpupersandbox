package Lesson1;

class PrM extends ProductPerson {

 protected PrM(String name, int age, Sex sex) {
        super(name, age, sex);
    }

    protected void sayHire() {
        System.out.println("Попросил выслать job offer");
    }

    protected void approavCandidate() {
        System.out.println("Одобрил кандидата на основании отчета интервьюера");
    }

    protected void rejectCandidate() {
        System.out.println("Отклонил кандидата на основании отчета интервьюера");
    }

    protected void getCv() {
        System.out.println("PM получил резюме кандидата");
    }
}
