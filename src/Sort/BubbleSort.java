package Sort;



public class BubbleSort {
    public BubbleSort(int[] arr1) {
        for (int i = arr1.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr1[j] > arr1[j + 1]) {
                    int tmp = arr1[j];
                    arr1[j] = arr1[j + 1];
                    arr1[j + 1] = tmp;
                }
            }
        }
    }
}